<?php
include_once('conf/koneksi.php');
if (isset($_SESSION["level"])) {
    header("location:dash.php");
}
if(isset($_POST['username'])){
 $querylogin = mysqli_query($kon, "select * from user where nama_user = '".$_POST['username']."'");
 if(mysqli_num_rows($querylogin) == 1){
  $udata = mysqli_fetch_assoc($querylogin);
  if(password_verify($_POST['password'], $udata['pass_user'])){
   header("location:dash.php");
   $_SESSION['id_user'] = $udata['id_user'];
   $_SESSION['username'] = $udata['nama_user'];
   $_SESSION['level'] = $udata['level'];
  } else {
   echo "<script>alert('Login Gagal! Username atau Password tidak ditemukan.');</script>";
  }
 } else {
  echo "<script>alert('Login Gagal! Username atau Password tidak ditemukan.');</script>";
 }
}
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Login Ke Sistem - SI Peramalan Penjualan Krupuk Senna</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
  <script src="assets/js/jquery.min.js"></script>
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
         <strong class="logo">Form Login</strong>
								</header>

							<!-- Content -->
								<section>
         <form name="login_pegawai" method="post" action="">
          <div class="row uniform">
           <div class="6u$ 12u$(small)">
            <label for="username_pegawai">Username</label>
            <input type="text" name="username" id="username_pegawai" maxlength="15"/>
           </div>
           <div class="6u$ 12u$(small)">
            <label for="password_pegawai">Password</label>
            <input type="password" name="password" id="password_pegawai" maxlength="20"/>
           </div>
           <ul class="actions">
            <li><button type="submit" name="login"class="button special"><span class="fa fa-power-off"></span> Login Ke Sistem</a></li>
           </ul>
          </div>
         </form>
								</section>

						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">
        <header class="major">
										<h2>Senna & Nixxa</h2>
									</header>
							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; PT Pratama Abadi Gemilang. Desain oleh <a href="https://html5up.net">HTML5 UP</a>.</p>
								</footer>

						</div>
					</div>

			</div>

		<!-- Scripts -->
			
			<script src="assets/js/skel.min.js')"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>