<?php
error_reporting(E_ALL ^ E_DEPRECATED);
session_start();
include('class.ViewAsset.php');
//$user = 'masizhan_prj';
$user = 'root';
//$pass = 'krupukcapanakbotak';
$pass = '';
$host = 'localhost';
//$db = 'masizhan_rmljual';
$db = 'ramaljual';

$kon = mysqli_connect($host,$user,$pass,$db);

//////////
function tanggal($dt,$with_timestamp=false){
 //format harus yyyy-mm-dd
 $bulan=array(
  "00" => "N/A",
  "01" => "Januari",
  "02" => "Februari",
  "03" => "Maret",
  "04" => "April",
  "05" => "Mei",
  "06" => "Juni",
  "07" => "Juli",
  "08" => "Agustus",
  "09" => "September",
  "10" => "Oktober",
  "11" => "November",
  "12" => "Desember"
 );
 $date=explode("-",$dt);
 $tahun=substr($date[2],0,2); //fix date with timestamp format
 $tanggal=$tahun." ".$bulan[$date[1]]." ".$date[0];
 if($with_timestamp){
  $tanggal .= " ".substr($date[2],3);
 }
 return $tanggal;
}

$global_bulan=array(
  "01" => "Januari",
  "02" => "Februari",
  "03" => "Maret",
  "04" => "April",
  "05" => "Mei",
  "06" => "Juni",
  "07" => "Juli",
  "08" => "Agustus",
  "09" => "September",
  "10" => "Oktober",
  "11" => "November",
  "12" => "Desember"
 );

function phash($pass){
 $pass = password_hash($pass, PASSWORD_DEFAULT);
 return $pass;
}

function hakAkses($level_akses=array(), $redir=true){
 $pass = 0;
 foreach($level_akses as $lvl){
  if($_SESSION['level'] == $lvl){
   $pass++;
  }
 }
 
 if($pass > 0){
  return true;
 } else {
  if($redir){
   echo "<script>alert('Anda tidak memiliki akses ke menu ini');document.location.href='dash.php';</script>";
  }
  return false;
 }
}


?>