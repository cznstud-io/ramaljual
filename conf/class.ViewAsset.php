<?php
class ViewAsset{
 public $inlinejs = [];
 
 function set_inline($content){
  $this->inlinejs[] = $content;
 }
 
 function get_inline(){
  return implode('\n', $this->inlinejs);
 }
 
 function reset_inline(){
  $this->inlinejs = [];
 }
}

$vws = new ViewAsset();
?>