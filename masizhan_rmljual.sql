-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 05 Mar 2018 pada 16.37
-- Versi server: 10.1.24-MariaDB-cll-lve
-- Versi PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `masizhan_rmljual`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(25) NOT NULL,
  `harga_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `harga_barang`) VALUES
(1, 'Udang Delux 380gr', 10000),
(2, 'Udang Delux 500gr', 17000),
(3, 'Udang Super 380gr', 12000),
(4, 'Senna Sayur 380gr', 20000),
(5, 'Nixxa Ikan 380gr ', 10000),
(6, 'Nixxa Premium 380gr', 0),
(7, 'Nixxa Supreme 380gr', 0),
(8, 'Nixxa Bawang 380gr', 0),
(9, 'Senna Ikan 380gr', 0),
(10, 'Nixxa Pizza 380gr', 0),
(11, 'Nixxa BBQ 380gr', 0),
(12, 'Kerupukku Udang 250gr', 0),
(13, 'Kerupukku Ikan 250gr', 0),
(14, 'Kerupukku Bawang 250gr', 0),
(15, 'Kerupukku Udang 200gr', 0),
(16, 'Kerupukku Ikan 200gr', 0),
(17, 'Kerupukku Bawang 200gr', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur`
--

CREATE TABLE `faktur` (
  `id_faktur` int(11) NOT NULL,
  `tgl_faktur` datetime NOT NULL,
  `id_pelanggan` varchar(25) NOT NULL,
  `jenis_faktur` tinyint(4) NOT NULL,
  `total_faktur` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `faktur`
--

INSERT INTO `faktur` (`id_faktur`, `tgl_faktur`, `id_pelanggan`, `jenis_faktur`, `total_faktur`, `id_user`) VALUES
(3, '2016-08-01 00:00:00', 'PABRIK', 0, 23370000, 1),
(6, '2016-07-01 00:00:00', 'PABRIK', 0, 33564000, 1),
(8, '2016-04-01 00:00:00', 'Pabrik', 0, 30720000, 1),
(10, '2016-01-01 00:00:00', 'PABRIK', 0, 42918000, 1),
(12, '2016-10-01 00:00:00', 'PABRIK', 0, 13500000, 1),
(17, '2016-11-01 00:00:00', 'PABRIK', 0, 10800000, 1),
(20, '2016-06-01 00:00:00', 'PABRIK', 0, 16140000, 1),
(24, '2016-05-01 00:00:00', 'PABRIK', 0, 27960000, 1),
(26, '2016-03-01 00:00:00', 'coba', 0, 31800000, 1),
(30, '2016-01-06 00:00:00', 'GIANT P.BARU METROPOLITAN', 1, 0, 1),
(31, '2016-01-06 00:00:00', '999 MINI SWALAYAN', 1, 150000, 1),
(32, '2016-01-06 00:00:00', 'MINI MARKET JUMBO MART', 1, 0, 1),
(33, '2016-01-06 00:00:00', 'ANTON LUCKY', 1, 222000, 1),
(34, '2016-01-06 00:00:00', 'TK. SRI UTAMI', 1, 0, 1),
(35, '2016-01-06 00:00:00', 'TK. JENNY JAYA', 1, 0, 1),
(36, '2016-01-06 00:00:00', 'KD. YUNUS', 1, 0, 1),
(37, '2016-01-06 00:00:00', 'TK. KHESSYFA', 1, 0, 1),
(38, '2016-01-07 00:00:00', 'PT. MATAHARI PUTRA PRIMA,', 1, 0, 1),
(39, '2016-01-07 00:00:00', 'PT. DINAMIKA BUAH NUSANTA', 1, 4080000, 1),
(40, '2016-01-07 00:00:00', 'K O N T A N ( KARYAWAN )', 1, 0, 1),
(41, '2016-01-07 00:00:00', 'TK. YENI', 1, 0, 1),
(42, '2016-01-07 00:00:00', 'TK. HARAPAN BUNDO', 1, 0, 1),
(43, '2016-01-07 00:00:00', 'KD. HARIAN ROMI', 1, 0, 1),
(44, '2016-01-07 00:00:00', 'KD. SAIYO / EDI', 1, 0, 1),
(45, '2016-01-07 00:00:00', 'SWALAYAN TOP', 1, 834000, 1),
(46, '2016-01-07 00:00:00', 'SWALAYAN DEAN NETWORK', 1, 708000, 1),
(47, '2016-01-07 00:00:00', 'TOSERBA ROKAN JAYA', 1, 0, 1),
(48, '2016-01-08 00:00:00', 'PT. TITANI ALAM SEMESTA', 1, 236000, 1),
(49, '2016-01-08 00:00:00', 'MOTORIST ADRIAS - TITANI', 1, 0, 1),
(50, '2016-01-08 00:00:00', 'TK. MARCI', 1, 0, 1),
(51, '2016-01-08 00:00:00', 'KD. BU NUR', 1, 0, 1),
(53, '2016-01-08 00:00:00', 'TK. BINA JAYA/ACAI', 1, 0, 1),
(54, '2016-01-08 00:00:00', 'TK. TARUKO', 1, 0, 1),
(55, '2016-01-08 00:00:00', 'TK. SALIBU', 1, 0, 1),
(56, '2016-01-08 00:00:00', 'K O N T A N ( KARYAWAN )', 1, 0, 1),
(57, '2016-01-08 00:00:00', 'TK. ILHAM JAYA', 1, 0, 1),
(58, '2016-01-08 00:00:00', 'TK. ANTO', 1, 0, 1),
(59, '2016-01-08 00:00:00', 'PT. TITANI ALAM SEMESTA', 1, 20000, 1),
(60, '2016-01-11 00:00:00', 'PT. DINAMIKA BUAH NUSANTA', 1, 10344000, 1),
(61, '2016-01-12 00:00:00', 'GIANT NANGKA PEKANBARU', 1, 0, 1),
(62, '2016-01-12 00:00:00', 'SWALAYAN PASAR BUAH 88', 1, 120000, 1),
(63, '2016-01-12 00:00:00', 'K O N T A N ( KARYAWAN )', 1, 0, 1),
(64, '2016-01-12 00:00:00', 'TK. SINGGALANG', 1, 0, 1),
(65, '2016-01-12 00:00:00', 'TK. MADRAS ALI', 1, 0, 1),
(66, '2016-01-12 00:00:00', 'PT. DINAMIKA BUAH NUSANTA', 1, 1080000, 1),
(67, '2016-01-12 00:00:00', 'SWALAYAN PASAR BUAH 88', 1, 0, 1),
(68, '2016-01-12 00:00:00', 'PT. DINAMIKA BUAH NUSANTA', 1, 150000, 1),
(69, '2016-01-13 00:00:00', 'K O N T A N ( KARYAWAN )', 1, 0, 1),
(70, '2016-01-13 00:00:00', 'TK. BATUBARA', 1, 0, 1),
(71, '2016-01-13 00:00:00', 'KD. SAYUR SEPAKAT BARU', 1, 0, 1),
(72, '2016-01-13 00:00:00', 'TK. MAKMUR BERSAUDARA', 1, 0, 1),
(73, '2016-01-13 00:00:00', 'SWALAYAN MAMA MIA 3', 1, 0, 1),
(74, '2016-01-13 00:00:00', 'K.O.N.T.A.N', 1, 0, 1),
(75, '2016-01-13 00:00:00', 'PT. MATAHARI PUTRA PRIMA,', 1, 0, 1),
(76, '2016-01-14 00:00:00', 'K O N T A N ( KARYAWAN )', 1, 0, 1),
(77, '2016-01-14 00:00:00', 'METRO PLAZA PASAR SWALAYA', 1, 150000, 1),
(78, '2016-01-14 00:00:00', 'TK. FAJAR', 1, 0, 1),
(79, '2016-01-14 00:00:00', 'PT. DAILY SUKSES SEJAHTER', 1, 0, 1),
(80, '2016-01-14 00:00:00', 'K O N T A N ( KARYAWAN )', 1, 0, 1),
(81, '2016-01-14 00:00:00', 'PT. MATAHARI PUTRA PRIMA,', 1, 0, 1),
(82, '2016-01-15 00:00:00', 'TK. PUTRA 2', 1, 0, 1),
(83, '2016-01-15 00:00:00', 'KD. AAS', 1, 0, 1),
(84, '2016-01-15 00:00:00', 'MALAYA SWALAYAN', 1, 0, 1),
(85, '2016-01-16 00:00:00', 'TK. NAFA', 1, 0, 1),
(86, '2016-01-16 00:00:00', 'TK. NAZHIFAH', 1, 0, 1),
(87, '2016-01-16 00:00:00', 'TK. WIDI', 1, 0, 1),
(88, '2016-01-16 00:00:00', 'TOSERBA FAJRI MART', 1, 20000, 1),
(89, '2016-01-18 00:00:00', 'PT. ARTHA SEMBILAN INDO', 1, 360000, 1),
(90, '2016-01-18 00:00:00', 'MOTORIST DEDI HARYONO - T', 1, 0, 1),
(91, '2016-01-18 00:00:00', 'TK. KEMBAR', 1, 0, 1),
(92, '2016-01-18 00:00:00', 'KD. MAK DAVID', 1, 0, 1),
(93, '2016-01-18 00:00:00', 'PT. ARTHA SEMBILAN INDO', 1, 0, 1),
(94, '2016-01-19 00:00:00', 'TK. SABAR MENANTI', 1, 0, 1),
(95, '2016-01-19 00:00:00', 'MINI MARKET CENDANA MART', 1, 330000, 1),
(96, '2016-01-19 00:00:00', 'PT. DAILY SUKSES SEJAHTER', 1, 150000, 1),
(97, '2016-01-19 00:00:00', 'K O N T A N ( KARYAWAN )', 1, 0, 1),
(98, '2016-01-19 00:00:00', 'PT. MATAHARI PUTRA PRIMA,', 1, 960000, 1),
(99, '2016-01-20 00:00:00', 'KOPERASI KARYAWAN PT.BANK', 1, 120000, 1),
(100, '2016-01-20 00:00:00', 'SWALAYAN MAMMA MIA 2', 1, 0, 1),
(101, '2016-01-20 00:00:00', 'TK. BINTANG MAS', 1, 0, 1),
(102, '2016-01-20 00:00:00', 'TK. JAY', 1, 0, 1),
(103, '2016-01-20 00:00:00', 'KD. EKO I', 1, 0, 1),
(104, '2016-01-20 00:00:00', 'GIANT NANGKA PEKANBARU', 1, 300000, 1),
(105, '2016-01-20 00:00:00', 'K O N T A N ( KARYAWAN )', 1, 0, 1),
(106, '2016-01-20 00:00:00', 'PT. MATAHARI PUTRA PRIMA ', 1, 0, 1),
(107, '2016-01-20 00:00:00', 'PT. DINAMIKA BUAH NUSANTA', 1, 900000, 1),
(108, '2016-01-20 00:00:00', 'GIANT NANGKA PEKANBARU', 1, 0, 1),
(109, '2016-01-21 00:00:00', 'K O N T A N ( KARYAWAN )', 1, 0, 1),
(110, '2016-01-21 00:00:00', 'TK. KARYA SUKSES MANDIRI', 1, 0, 1),
(111, '2016-01-21 00:00:00', 'TK. EZA GAME', 1, 0, 1),
(112, '2016-01-21 00:00:00', 'TK. AWAL USAHA', 1, 300000, 1),
(113, '2016-01-21 00:00:00', 'KD. DION', 1, 0, 1),
(114, '2016-01-21 00:00:00', 'TK. HANLIN', 1, 0, 1),
(115, '2016-01-21 00:00:00', 'TK. RIZKY', 0, 0, 1),
(116, '2016-01-21 00:00:00', 'TK. ZAM-ZAM', 1, 0, 1),
(117, '2016-01-22 00:00:00', 'MINI MARKET 99 MART', 1, 0, 1),
(118, '2016-01-22 00:00:00', 'TK. MULIA MAJU', 1, 0, 1),
(119, '2016-01-22 00:00:00', 'K.O.N.T.A.N', 1, 0, 1),
(120, '2016-01-22 00:00:00', 'MINI MARKET JUMBO MART', 1, 0, 1),
(121, '2016-01-23 00:00:00', 'SWALAYAN MAMMA MIA', 1, 51000, 1),
(122, '2016-01-23 00:00:00', 'SWALAYAN SUN', 1, 0, 1),
(123, '2016-01-23 00:00:00', 'MINI MARKET MULIA MART 2', 1, 0, 1),
(124, '2016-01-23 00:00:00', 'TK. LYNN', 1, 0, 1),
(125, '2016-01-23 00:00:00', 'KD. PSR MINI RANG SANGKA', 1, 0, 1),
(126, '2016-01-23 00:00:00', 'TK. SALIBU', 1, 0, 1),
(127, '2016-01-23 00:00:00', 'TK. NOVIA', 1, 0, 1),
(128, '2016-01-23 00:00:00', 'KD. NURUL', 1, 0, 1),
(129, '2016-01-23 00:00:00', 'KOPERASI AS-SHOFA', 1, 0, 1),
(130, '2016-01-23 00:00:00', 'TK. MURNI FAMILLY', 1, 0, 1),
(131, '2016-01-23 00:00:00', 'TK. KEYSA', 1, 0, 1),
(132, '2016-01-23 00:00:00', 'SWALAYAN GLOBAL', 1, 120000, 1),
(133, '2016-01-25 00:00:00', 'TK. ANGEL', 1, 0, 1),
(134, '2016-01-25 00:00:00', 'KD. HARIAN FAHMI', 1, 0, 1),
(135, '2016-01-26 00:00:00', 'TK. RAYHAN (SBL AG-21)', 1, 0, 1),
(136, '2016-01-26 00:00:00', 'KD. SIFAT', 1, 0, 1),
(137, '2016-01-26 00:00:00', 'TK. SHARUL', 1, 0, 1),
(138, '2016-01-26 00:00:00', 'TK. NAZAR', 1, 0, 1),
(139, '2016-01-26 00:00:00', 'MINI MARKET JUMBO MART 2', 1, 0, 1),
(140, '2016-01-26 00:00:00', 'KD. UMI', 1, 0, 1),
(141, '2016-01-26 00:00:00', 'K O N T A N ( KARYAWAN )', 1, 0, 1),
(142, '2016-01-27 00:00:00', 'TK. H. ZUL', 1, 0, 1),
(143, '2016-01-27 00:00:00', 'KD. HARIAN SYAHNA', 1, 0, 1),
(144, '2016-01-27 00:00:00', 'TK. MARYONO', 1, 0, 1),
(145, '2016-01-27 00:00:00', 'TK. RAJA PLASTIK', 1, 0, 1),
(146, '2016-01-27 00:00:00', 'TK. AINI', 1, 0, 1),
(147, '2016-01-27 00:00:00', 'TK. LIMA-SAUDARA', 1, 0, 1),
(148, '2016-01-27 00:00:00', 'TK. DUA-SAUDARA', 1, 0, 1),
(149, '2016-01-27 00:00:00', 'TK. BERKAH II', 1, 0, 1),
(150, '2016-01-27 00:00:00', 'PT. MATAHARI PUTRA PRIMA,', 1, 0, 1),
(151, '2016-01-28 00:00:00', 'KD. HARIAN RERE', 1, 0, 1),
(152, '2016-01-28 00:00:00', 'GIANT NANGKA PEKANBARU', 1, 0, 1),
(153, '2016-01-28 00:00:00', 'MANDIRI IDAYU MINIMART', 1, 60000, 1),
(154, '2016-01-28 00:00:00', 'PT. BANGUN SEJAHTERA BERS', 1, 0, 1),
(155, '2016-01-29 00:00:00', 'KD. REZEKI PUTRI', 1, 0, 1),
(156, '2016-01-29 00:00:00', 'TK. JENNY JAYA', 1, 0, 1),
(157, '2016-01-29 00:00:00', 'KD. HARIAN ROMI', 1, 0, 1),
(158, '2016-01-29 00:00:00', 'GIANT P.BARU METROPOLITAN', 1, 300000, 1),
(159, '2016-01-29 00:00:00', 'MOTORIST DEDI HARYONO - T', 1, 0, 1),
(160, '2016-01-29 00:00:00', 'FAMILY SWALAYAN', 1, 0, 1),
(161, '2016-01-30 00:00:00', 'SWALAYAN MAMMA MIA', 1, 0, 1),
(162, '2016-01-30 00:00:00', 'TOSERBA SINGGALANG JAYA', 1, 150000, 1),
(163, '2016-01-30 00:00:00', 'TK. MARTHA', 1, 780000, 1),
(164, '2016-01-30 00:00:00', 'PT. BANGUN SEJAHTERA BERS', 1, 0, 1),
(165, '2016-01-30 00:00:00', 'PT. DINAMIKA BUAH NUSANTA', 1, 1200000, 1),
(166, '2016-01-30 00:00:00', 'TK. MARINA', 1, 0, 1),
(167, '2016-01-30 00:00:00', 'TK. BINA JAYA/ACAI', 1, 0, 1),
(168, '2016-01-30 00:00:00', 'HAWAII SWALAYAN RUMBAI', 1, 82000, 1),
(169, '2016-01-30 00:00:00', 'PT. MATAHARI PUTRA PRIMA,', 1, 630000, 1),
(170, '2016-02-12 00:00:00', 'FEBRUARI', 1, 2960000, 1),
(171, '2016-03-12 00:00:00', 'MARET', 1, 5980000, 1),
(172, '2016-04-12 00:00:00', 'APRIL', 1, 5040000, 1),
(173, '2016-05-12 00:00:00', 'MEI', 1, 5360000, 1),
(174, '2016-06-12 00:00:00', 'JUNI', 1, 7500000, 1),
(175, '2016-07-12 00:00:00', 'JULI', 1, 600000, 1),
(176, '2016-08-12 00:00:00', 'AGUSTUS', 1, 4130000, 1),
(177, '2016-09-12 00:00:00', 'SEPTEMBER', 1, 3310000, 1),
(178, '2016-10-12 00:00:00', 'OKTOBER', 1, 4960000, 1),
(179, '2016-11-12 00:00:00', 'NOVEMBER', 1, 5400000, 1),
(180, '2016-12-12 00:00:00', 'DESEMBER', 1, 4870000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur_detail`
--

CREATE TABLE `faktur_detail` (
  `id` int(11) NOT NULL,
  `id_faktur` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `faktur_detail`
--

INSERT INTO `faktur_detail` (`id`, `id_faktur`, `id_barang`, `qty`) VALUES
(46, 10, 6, 215),
(47, 10, 7, 240),
(48, 10, 8, 305),
(49, 10, 9, 290),
(50, 10, 10, 196),
(51, 10, 11, 296),
(52, 10, 1, 772),
(53, 10, 2, 718),
(54, 10, 3, 576),
(55, 10, 4, 371),
(56, 10, 5, 866),
(57, 10, 12, 8515),
(58, 10, 13, 6844),
(59, 10, 14, 4051),
(60, 10, 15, 22013),
(61, 10, 16, 23134),
(62, 10, 17, 16409),
(83, 26, 6, 90),
(84, 26, 7, 90),
(85, 26, 10, 60),
(86, 26, 1, 600),
(87, 26, 3, 300),
(88, 26, 2, 600),
(89, 26, 4, 300),
(90, 26, 5, 600),
(91, 26, 15, 10920),
(92, 26, 16, 6720),
(93, 26, 17, 4200),
(94, 8, 6, 150),
(95, 8, 8, 180),
(96, 8, 11, 150),
(97, 8, 1, 750),
(98, 8, 3, 300),
(99, 8, 2, 360),
(100, 8, 4, 300),
(101, 8, 5, 750),
(102, 8, 12, 2520),
(103, 8, 14, 2520),
(104, 8, 15, 12600),
(105, 24, 1, 750),
(106, 24, 2, 480),
(107, 24, 3, 150),
(108, 24, 4, 150),
(109, 24, 5, 750),
(110, 24, 12, 1680),
(111, 24, 13, 1680),
(112, 24, 14, 840),
(113, 24, 15, 8400),
(114, 24, 16, 4200),
(115, 24, 17, 2520),
(116, 20, 6, 90),
(117, 20, 7, 90),
(118, 20, 9, 150),
(119, 20, 1, 300),
(120, 20, 2, 120),
(121, 20, 3, 300),
(122, 20, 4, 150),
(123, 20, 5, 450),
(124, 20, 12, 4200),
(125, 20, 13, 840),
(126, 20, 14, 840),
(127, 20, 15, 8400),
(128, 20, 16, 4200),
(129, 20, 17, 4200),
(130, 6, 6, 300),
(131, 6, 7, 180),
(132, 6, 8, 150),
(133, 6, 9, 180),
(134, 6, 1, 1050),
(135, 6, 2, 192),
(136, 6, 3, 150),
(137, 6, 4, 300),
(138, 6, 5, 1200),
(139, 6, 12, 1200),
(140, 6, 13, 400),
(141, 6, 14, 400),
(142, 6, 15, 4000),
(143, 3, 1, 750),
(144, 3, 2, 150),
(145, 3, 3, 360),
(146, 3, 5, 900),
(147, 3, 12, 2000),
(148, 3, 13, 1200),
(149, 3, 14, 1000),
(150, 3, 15, 3200),
(151, 3, 16, 1200),
(152, 3, 17, 1000),
(153, 12, 6, 90),
(154, 12, 7, 60),
(155, 12, 8, 150),
(156, 12, 1, 750),
(157, 12, 5, 600),
(158, 12, 12, 4400),
(159, 12, 13, 4000),
(160, 12, 14, 2400),
(161, 12, 15, 21000),
(162, 12, 16, 3600),
(163, 12, 17, 2000),
(164, 17, 7, 60),
(165, 17, 8, 90),
(166, 17, 9, 90),
(167, 17, 1, 300),
(168, 17, 3, 150),
(169, 17, 5, 600),
(170, 17, 12, 800),
(171, 17, 13, 800),
(172, 17, 14, 800),
(173, 17, 15, 12000),
(174, 17, 16, 8000),
(175, 17, 17, 2000),
(180, 30, 12, 200),
(181, 30, 13, 200),
(182, 30, 14, 120),
(183, 31, 12, 20),
(184, 31, 13, 20),
(185, 31, 14, 20),
(186, 31, 9, 15),
(187, 31, 1, 15),
(188, 32, 12, 80),
(189, 32, 13, 80),
(190, 33, 3, 6),
(191, 33, 1, 15),
(192, 33, 13, 10),
(193, 34, 15, 20),
(194, 34, 16, 10),
(195, 34, 17, 10),
(197, 36, 15, 20),
(198, 36, 17, 10),
(199, 36, 16, 10),
(200, 37, 15, 40),
(201, 38, 13, 40),
(202, 38, 14, 40),
(203, 39, 12, 40),
(204, 39, 13, 40),
(205, 39, 2, 240),
(208, 41, 15, 40),
(209, 41, 17, 40),
(210, 41, 16, 40),
(211, 42, 15, 40),
(212, 42, 16, 40),
(213, 43, 15, 20),
(214, 43, 16, 20),
(215, 44, 15, 40),
(216, 44, 16, 20),
(217, 44, 17, 20),
(218, 35, 15, 80),
(219, 45, 9, 15),
(220, 45, 1, 15),
(221, 45, 2, 12),
(222, 45, 4, 15),
(223, 45, 3, 15),
(224, 45, 14, 20),
(225, 45, 13, 20),
(226, 45, 12, 20),
(227, 40, 15, 15),
(228, 40, 17, 10),
(229, 46, 12, 12),
(230, 46, 13, 12),
(231, 46, 14, 12),
(232, 46, 9, 12),
(233, 46, 1, 12),
(234, 46, 3, 12),
(235, 46, 4, 12),
(236, 46, 2, 12),
(237, 47, 15, 40),
(238, 47, 16, 40),
(247, 48, 1, 4),
(248, 48, 3, 4),
(249, 48, 9, 4),
(250, 48, 4, 4),
(251, 48, 2, 4),
(252, 48, 12, 10),
(253, 48, 13, 10),
(254, 48, 14, 10),
(255, 49, 15, 1000),
(256, 49, 16, 600),
(257, 49, 17, 200),
(258, 50, 14, 20),
(259, 50, 13, 20),
(260, 50, 12, 20),
(261, 51, 15, 40),
(262, 51, 16, 40),
(266, 53, 15, 80),
(267, 53, 17, 40),
(268, 53, 16, 40),
(269, 54, 15, 10),
(270, 54, 17, 15),
(271, 54, 16, 15),
(272, 55, 15, 40),
(273, 55, 17, 20),
(274, 55, 16, 20),
(275, 56, 15, 40),
(276, 57, 15, 40),
(277, 58, 16, 30),
(278, 58, 17, 10),
(279, 59, 7, 2),
(280, 59, 6, 2),
(281, 59, 8, 2),
(282, 59, 5, 2),
(283, 59, 11, 2),
(284, 59, 10, 2),
(285, 60, 2, 432),
(286, 60, 4, 150),
(287, 61, 14, 200),
(288, 61, 13, 280),
(289, 61, 12, 280),
(290, 62, 12, 20),
(291, 62, 13, 20),
(292, 62, 14, 20),
(293, 62, 1, 12),
(294, 62, 9, 12),
(295, 63, 15, 50),
(296, 63, 16, 40),
(297, 64, 15, 80),
(298, 65, 15, 40),
(299, 66, 3, 90),
(300, 67, 8, 12),
(301, 68, 5, 15),
(302, 68, 11, 15),
(303, 69, 15, 5),
(304, 69, 17, 5),
(305, 69, 16, 5),
(306, 70, 15, 120),
(307, 70, 17, 40),
(308, 70, 16, 40),
(309, 71, 15, 20),
(310, 71, 17, 10),
(311, 71, 16, 10),
(312, 72, 15, 80),
(313, 72, 16, 40),
(314, 73, 12, 12),
(315, 73, 9, 6),
(316, 74, 15, 15),
(317, 74, 16, 10),
(318, 74, 17, 5),
(319, 75, 13, 40),
(320, 76, 15, 5),
(321, 76, 16, 5),
(322, 77, 12, 40),
(323, 77, 13, 40),
(324, 77, 14, 40),
(325, 77, 1, 15),
(326, 78, 9, 12),
(327, 78, 12, 12),
(328, 78, 14, 12),
(329, 78, 13, 12),
(330, 79, 13, 20),
(331, 79, 12, 20),
(332, 80, 15, 3),
(333, 80, 17, 3),
(334, 80, 16, 3),
(336, 81, 12, 80),
(337, 82, 15, 20),
(338, 82, 16, 20),
(339, 83, 15, 20),
(340, 83, 17, 10),
(341, 83, 16, 10),
(342, 84, 15, 40),
(344, 85, 15, 40),
(345, 86, 15, 40),
(346, 86, 16, 40),
(347, 87, 15, 80),
(348, 88, 15, 6),
(349, 88, 17, 6),
(350, 88, 16, 6),
(351, 88, 1, 2),
(352, 88, 9, 2),
(353, 89, 12, 20),
(354, 89, 14, 12),
(355, 89, 13, 12),
(356, 89, 9, 12),
(357, 89, 1, 12),
(358, 89, 4, 12),
(359, 90, 15, 1000),
(360, 90, 17, 400),
(361, 90, 16, 600),
(362, 91, 15, 15),
(363, 91, 16, 15),
(364, 91, 17, 10),
(365, 92, 15, 10),
(366, 92, 16, 20),
(367, 92, 17, 10),
(368, 93, 8, 6),
(369, 94, 15, 40),
(370, 94, 16, 40),
(371, 95, 15, 40),
(372, 95, 1, 15),
(373, 95, 3, 15),
(374, 96, 9, 15),
(375, 96, 1, 15),
(376, 97, 15, 19),
(377, 97, 16, 2),
(378, 97, 17, 9),
(379, 98, 9, 60),
(380, 98, 1, 60),
(381, 98, 3, 30),
(382, 99, 12, 12),
(383, 99, 9, 12),
(384, 99, 4, 6),
(385, 100, 12, 40),
(386, 101, 15, 35),
(387, 101, 16, 5),
(388, 102, 15, 20),
(389, 102, 17, 20),
(390, 102, 16, 20),
(394, 104, 12, 200),
(395, 104, 1, 30),
(396, 105, 15, 2),
(397, 103, 15, 20),
(398, 103, 17, 20),
(399, 103, 16, 20),
(400, 106, 9, 30),
(401, 106, 13, 40),
(402, 107, 1, 90),
(403, 107, 9, 30),
(404, 108, 8, 30),
(405, 109, 15, 80),
(406, 110, 15, 40),
(407, 110, 16, 40),
(408, 111, 15, 20),
(409, 111, 16, 20),
(410, 112, 1, 30),
(411, 112, 9, 12),
(412, 113, 15, 20),
(413, 113, 16, 20),
(414, 114, 15, 13),
(415, 115, 15, 15),
(416, 115, 16, 15),
(417, 115, 17, 10),
(418, 116, 15, 9),
(419, 116, 16, 9),
(420, 116, 17, 8),
(421, 117, 12, 6),
(422, 117, 13, 6),
(423, 117, 14, 6),
(424, 118, 15, 40),
(425, 118, 17, 40),
(426, 118, 16, 40),
(427, 119, 15, 2400),
(428, 119, 17, 2400),
(429, 119, 16, 2400),
(430, 120, 13, 80),
(431, 120, 12, 120),
(432, 120, 9, 30),
(433, 121, 12, 40),
(434, 121, 2, 3),
(435, 122, 12, 10),
(436, 122, 14, 10),
(437, 122, 13, 10),
(438, 123, 15, 20),
(439, 123, 16, 20),
(440, 123, 17, 20),
(441, 124, 14, 20),
(442, 124, 13, 20),
(443, 124, 12, 20),
(444, 124, 9, 15),
(445, 125, 15, 40),
(446, 125, 16, 40),
(447, 126, 15, 40),
(448, 126, 16, 40),
(449, 127, 15, 20),
(450, 127, 17, 10),
(451, 127, 16, 10),
(452, 128, 15, 60),
(453, 128, 16, 20),
(454, 129, 16, 40),
(455, 130, 15, 20),
(456, 130, 17, 10),
(457, 130, 16, 10),
(458, 131, 15, 15),
(459, 131, 17, 10),
(460, 131, 16, 15),
(461, 132, 12, 12),
(462, 132, 13, 12),
(463, 132, 1, 12),
(464, 132, 9, 12),
(468, 135, 15, 20),
(469, 135, 16, 20),
(470, 133, 15, 40),
(471, 133, 16, 20),
(472, 134, 15, 40),
(473, 136, 16, 40),
(474, 137, 15, 40),
(475, 137, 16, 20),
(476, 137, 17, 20),
(477, 138, 15, 200),
(478, 139, 12, 40),
(479, 140, 15, 20),
(480, 140, 16, 20),
(481, 141, 15, 3),
(482, 142, 15, 10),
(483, 142, 17, 10),
(484, 142, 16, 10),
(485, 143, 15, 20),
(486, 143, 17, 10),
(487, 143, 16, 10),
(488, 144, 15, 40),
(489, 145, 15, 20),
(490, 145, 16, 15),
(491, 145, 17, 5),
(492, 146, 15, 20),
(493, 146, 16, 20),
(494, 147, 15, 40),
(495, 147, 17, 20),
(496, 147, 16, 20),
(497, 148, 15, 20),
(498, 148, 16, 20),
(499, 149, 15, 40),
(500, 149, 16, 40),
(501, 150, 14, 40),
(502, 151, 17, 15),
(503, 151, 16, 11),
(504, 152, 14, 200),
(505, 152, 13, 200),
(506, 152, 12, 200),
(507, 153, 12, 12),
(508, 153, 9, 12),
(509, 153, 1, 6),
(510, 154, 13, 40),
(511, 154, 12, 40),
(512, 154, 14, 40),
(513, 154, 9, 30),
(514, 155, 15, 20),
(515, 155, 16, 6),
(516, 156, 15, 80),
(517, 157, 15, 40),
(518, 158, 1, 30),
(519, 158, 12, 80),
(520, 158, 13, 80),
(521, 159, 15, 600),
(522, 159, 16, 200),
(523, 159, 17, 200),
(524, 160, 15, 40),
(525, 161, 9, 9),
(526, 162, 12, 40),
(527, 162, 13, 20),
(528, 162, 14, 20),
(529, 162, 1, 15),
(530, 163, 14, 40),
(531, 163, 13, 40),
(532, 163, 12, 40),
(533, 163, 4, 30),
(534, 163, 9, 30),
(535, 163, 3, 15),
(536, 164, 12, 20),
(537, 164, 14, 12),
(538, 164, 13, 36),
(539, 164, 9, 6),
(540, 165, 4, 60),
(541, 166, 15, 40),
(542, 166, 16, 10),
(543, 167, 17, 40),
(558, 168, 14, 20),
(559, 168, 13, 20),
(560, 168, 9, 15),
(561, 168, 1, 1),
(562, 168, 3, 6),
(563, 169, 12, 40),
(564, 169, 13, 80),
(565, 169, 1, 27),
(566, 169, 3, 30),
(567, 169, 6, 60),
(568, 170, 1, 296),
(569, 171, 1, 598),
(570, 172, 1, 504),
(571, 173, 1, 536),
(572, 174, 1, 750),
(573, 175, 1, 60),
(574, 176, 1, 413),
(575, 177, 1, 331),
(576, 178, 1, 496),
(577, 179, 1, 540),
(578, 180, 1, 487);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_pegawai` varchar(35) NOT NULL,
  `nama_user` varchar(25) NOT NULL,
  `pass_user` varchar(100) NOT NULL,
  `level` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama_pegawai`, `nama_user`, `pass_user`, `level`) VALUES
(1, 'Mamang Hori', 'admin', '$2y$10$wxYPVj181SEXAcZi5U.4s.GkDzrceWBC/XEhvDENXXJYJaE0CrRcy', 0),
(2, 'Rudi Hartono', 'rudi', '$2y$10$dzxROWjS.cG/u3uhqqdVBuTr2LJzYee/bdmJBqduS3EoMJ0BqhDlO', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indeks untuk tabel `faktur`
--
ALTER TABLE `faktur`
  ADD PRIMARY KEY (`id_faktur`);

--
-- Indeks untuk tabel `faktur_detail`
--
ALTER TABLE `faktur_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `faktur`
--
ALTER TABLE `faktur`
  MODIFY `id_faktur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT untuk tabel `faktur_detail`
--
ALTER TABLE `faktur_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=579;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
