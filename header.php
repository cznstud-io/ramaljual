<?php
if (!isset($_SESSION["username"])) {
header("location:login.php");
}
else if (!isset($_SESSION["level"]) ) {
header("location:login.php");
}
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>SI Peramalan Penjualan</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css') ?>" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css') ?>" /><![endif]-->
  <script src="assets/js/jquery-3.2.1.min.js"></script>
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
         <strong class="logo">SI Peramalan Penjualan Menggunakan Metode <em>Weight Moving Average</em></strong>
								</header>

							<!-- Content -->
								<section>
         <?php if(isset($parent)) {  ?>
         <ul class="actions">
          <li><a href="<?= $parent_link ?>" class="button small"><span class="fa fa-arrow-left"></span> Kembali ke <?= $parent ?></a></li>
         </ul>
         <?php } 
         if(isset($_SESSION['message'])){?>
         <div class="alert alert-<?= $_SESSION['class'] ?>">
         <strong><?= $_SESSION['alert'] ?></strong>
         <?= $_SESSION['message'] ?> </div>
         <?php 
         }