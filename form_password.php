<?php
include_once('conf/koneksi.php');

if(isset($_POST['id_user'])){ // PASS 1: ID User ada di Database
  $state = false;
  $id_user = (int)$_POST['id_user'];
  if($_SESSION['level'] == 0 && $_SESSION['id_user'] != $id_user){
   $query = mysqli_query($kon, "select * from user where id_user='".$_SESSION['id_user']."'");
  } else {
   $query = mysqli_query($kon, "select * from user where id_user='".$id_user."'");
  }
  $userdata = mysqli_fetch_array($query) or die('Gagal');
  if(password_verify($_POST['pass_user'], $userdata['pass_user'])){ // PASS 2: Password Sesuai dengan di Database
   if($_POST['pass_user_new'] === $_POST['pass_user_konfirm']) { // PASS 3: Pengguna telah konfirmasi password barunya
    $pass_user = password_hash($_POST['pass_user_new'], PASSWORD_DEFAULT);
    $query_update = mysqli_query($kon, "update user set pass_user='".$pass_user."' where id_user='".$id_user."'");
    if($query_update){
     $state = true;
    }
   } 
  }
  $redir = 'lihat_pengguna.php';
  if($_SESSION['level'] == 1){
   $redir = "dash.php";
  }
  if($state){
   echo "<script>alert('Password berhasil diubah'); document.location.href='".$redir."'; </script>";
  } else {
   echo "<script>alert('Password gagal diubah'); document.location.href='".$redir."'; </script>";
  }
}
$id_user = (int)$_GET['id'];
$query = mysqli_query($kon, "select * from user where id_user='".$id_user."'");
$userdata = mysqli_fetch_array($query) or die('Gagal');
include('header.php');
?>
<h3>Ubah Password untuk Pengguna <em><?= $userdata['nama_user'] ?></em></h3>
<form method="POST" action="form_password.php" name="password">
 <input type="hidden" name="id_user" value="<?= isset($userdata['id_user'])?$userdata['id_user']:'' ?>" />
 <div class="row uniform">
  <div class="8u$ 12u$(small)">
   <?php if($_SESSION['level'] == 0 && $_SESSION['id_user'] != $id_user){ ?>
   <label>Password Anda sebagai Administrator</label>
   <?php } else { ?>
   <label>Password Sekarang</label>
   <?php } ?>
   <input type="password" name="pass_user" value="" placeholder="Password" maxlength="25" required/>
  </div>
  <div class="8u$ 12u$(small)">
   <label>Password Baru</label>
   <input type="password" name="pass_user_new" value="" id="pass_new" placeholder="Password Baru" maxlength="25" required/>
  </div>
  <div class="8u$ 12u$(small)">
   <label>Konfirmasi Password Baru</label>
   <input type="password" name="pass_user_konfirm" value="" id="pass_konf" placeholder="Konfirmasi Password Baru" maxlength="25" required/>
  </div>
  <ul class="actions">
   <li><button type="submit" name="simpan" class="button special">Simpan</button></li>
  </ul>
 </div>
</form>
<?php
ob_start(); ?>
<script>
 $(document).ready(function() {
  $("#pass_konf").on('change', function() {
   if($("#pass_new").val() !== $("#pass_konf").val()){
    alert('Password yang Anda masukkan tidak sesuai. Pastikan Anda mengingat password baru Anda dan masukkan di kolom "Konfirmasi Password Baru"');
    $("#pass_konf").val("");
    $("#pass_konf").focus();
   }
  })
 });
</script>
<?php
$scr = ob_get_clean();
$vws->set_inline($scr);
include('footer.php');