<?php
include_once('conf/koneksi.php');
if(isset($_GET['edit'])){
  $query = mysqli_query($kon, "select * from user where id_user='".$_GET['edit']."'");
  $row = mysqli_fetch_assoc($query);
} else if (isset($_POST['id_user'])){
 if($_POST['id_user'] === ''){
  $query = mysqli_query($kon, "insert into user values('','".$_POST['nama_pegawai']."','".$_POST['nama_user']."','".phash($_POST['pass_user'])."','".$_POST['level']."')");
  if($query){
   echo "<script>alert('Pengguna berhasil ditambahkan!');\n document.location = 'lihat_pengguna.php'</script>";
  } else {
   echo "<script>alert('Terdapat Kesalahan Penambahan. Kode Error: '".mysqli_error($kon).");\n document.location = 'lihat_pengguna.php'</script>";
  }
 } else {
  $redir = 'lihat_pengguna.php';
  if($_SESSION['level'] == 1){
   $redir = "form_pengguna.php?edit=".$_SESSION['id_user'];
  }
  $query = mysqli_query($kon,"update user set nama_pegawai='".$_POST['nama_pegawai']."', nama_user='".$_POST['nama_user']."', level=".$_POST['level']." where id_user='".$_POST['id_user']."'");
  if($query){
   echo "<script>alert('Data berhasil diperbarui');\n document.location = '".$redir."'</script>";
  } else {
   echo "<script>alert('Terdapat Kesalahan dalam pembaruan data. Kode Error: '".mysqli_error($kon)."');\n document.location = '".$redir."'</script>";
  }
 }
}
include('header.php');
if(!isset($_GET['edit'])){
  hakAkses(array(0));
}
?>

<form method="POST" action="form_pengguna.php" name="kategori">
 <input type="hidden" name="id_user" value="<?= isset($row['id_user'])?$row['id_user']:'' ?>" />
 <div class="row uniform">
  <div class="8u$ 12u$(small)">
   <label>Nama Pegawai</label>
   <input type="text" name="nama_pegawai" value="<?= isset($row['nama_pegawai'])?$row['nama_pegawai']:'' ?>" placeholder="Nama Pegawai" maxlength="25" required/>
  </div>
  <div class="8u$ 12u$(small)">
   <label>Nama Pengguna</label>
   <input type="text" name="nama_user" value="<?= isset($row['nama_user'])?$row['nama_user']:'' ?>" placeholder="Nama Pengguna" maxlength="25" required/>
  </div>
  <?php if(!isset($_GET['edit'])) { ?>
  <div class="8u$ 12u$(small)">
   <label>Password</label>
   <input type="password" name="pass_user" value="" placeholder="Password" maxlength="25" required/>
  </div>
  <?php } 
  if($_SESSION['level'] == 0) { ?>
  <div class="8u$ 12u$(small)">
   <label>Level Pengguna</label>
   <select name="level">
    <option value="1" <?= isset($row['level'])?($row['level']==1?'selected':''):'' ?>>Pimpinan</option>
    <option value="0" <?= isset($row['level'])?($row['level']==0?'selected':''):'' ?>>Administrator</option>
   </select>
  </div>
  <?php } else { ?>
  <div class="8u$ 12u$(small)">
   <input type="hidden" name="level" value="1" />
  </div>
  <?php } ?>
  <ul class="actions">
   <li><button type="submit" name="simpan" class="button special">Simpan</button></li>
  </ul>
 </div>
</form>
<?php
include('footer.php');