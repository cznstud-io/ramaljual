<?php
include_once('conf/koneksi.php');
include_once('wma.php');
$id_barang = isset($_POST['id_barang'])?$_POST['id_barang']:"";
$barang = array();
$bulan = isset($_POST['bulan']) ? $_POST['bulan'] : date('m');
$tahun = isset($_POST['tahun']) ? $_POST['tahun'] : date('Y');
$valuewma = isset($_POST['valuewma']) ? $_POST['valuewma'] : 3;

$querybarang = mysqli_query($kon,"select id_barang,nama_barang from barang order by substring(nama_barang, -5, 3) asc");
while($row=mysqli_fetch_array($querybarang)){
 $barang[$row['id_barang']] = array(
  'nama_barang' => $row['nama_barang']
 );
}
if($id_barang !== ""){
  $queryjual =  mysqli_query($kon,"SELECT MONTH(faktur.tgl_faktur) as bulan, YEAR(faktur.tgl_faktur) as tahun, (YEAR(faktur.tgl_faktur) * 12 + MONTH(faktur.tgl_faktur) - 1) as serialmonth, barang.id_barang,sum(if(faktur.jenis_faktur=1,qty,0)) as penjualan FROM faktur_detail right join barang on faktur_detail.id_barang = barang.id_barang,faktur where faktur.id_faktur = faktur_detail.id_faktur and barang.id_barang = faktur_detail.id_barang and faktur_detail.id_barang = '".$id_barang."' and (YEAR(faktur.tgl_faktur) * 12 + MONTH(faktur.tgl_faktur) - 1) < (".$tahun." * 12 + ".$bulan." - 1) group by (YEAR(faktur.tgl_faktur) * 12 + MONTH(faktur.tgl_faktur) - 1) order by serialmonth desc"); 
  $barang[$id_barang]['penjualan'] = [];
  $barang[$id_barang]['bulan'] = [];
  while($row2=mysqli_fetch_array($queryjual)){
   $barang[$id_barang]['bulan'][] = $row2['serialmonth'];
   $barang[$id_barang]['penjualan'][$row2['serialmonth']] = $row2['penjualan'];
  }
  $jumlah_data = count($barang[$id_barang]['penjualan']);
  if($jumlah_data < 2){
   $barang[$id_barang]['prediksi_jual'] = 0;
  } else {
   //echo var_dump($key);
   //echo var_dump($jumlah_data);
   $periode_prediksi = $tahun * 12 + $bulan - 1;
   $cobaperiode = [];
   for($i=min($barang[$id_barang]['bulan']);$i < $periode_prediksi;$i++){
    $barang[$id_barang]['penjualan'][$i] = isset($barang[$id_barang]['penjualan'][$i])?$barang[$id_barang]['penjualan'][$i]:0;
   }
   krsort($barang[$id_barang]['penjualan']);
   //echo var_dump($barang[$id_barang]['penjualan']);
   $max_bulan_data = max($barang[$id_barang]['bulan']);
   //echo var_dump($max_bulan_data);
   $bulan_prediksi = $jumlah_data + $periode_prediksi - $max_bulan_data;
   //echo var_dump($bulan_prediksi);
   $wma->fit($barang[$id_barang]['penjualan']);
   foreach([3,5] as $valuewma){
    $weight_prediksi = $jumlah_data>$valuewma?$valuewma:$jumlah_data;
    //echo var_dump($weight_prediksi);
    //$weight_prediksi = 5;
    //echo $bulan_prediksi." ".$weight_prediksi;
    $barang[$id_barang]['prediksi_jual'][$valuewma] = $wma->predict($bulan_prediksi,$weight_prediksi);
   }
  }
}

//echo var_dump($barang);
include('header.php');

?>

<form name="pilihtgl" action="" method="POST">
<div class="row uniform">
<div class="4u 12u">
  <label>Pilih Bulan</label>
  <select name="bulan" class="form-control">
   <?php foreach($global_bulan as $key=>$val){ ?>
   <option value="<?= $key ?>" <?= ($bulan==$key)?'selected':''; ?>><?= $val ?></option>
   <?php } ?>
  </select>
  
</div>
<div class="4u 12u">
  <label>Pilih Tahun</label>
  <select name="tahun" class="form-control">
   <?php foreach(range(2016,date('Y')+5) as $val){ ?>
   <option value="<?= $val ?>" <?= ($tahun==$val)?'selected':''; ?>><?= $val ?></option>
   <?php } ?>
  </select>
</div>
<div class="4u 12u">
  <!--label>Peramalan</label>
  <select name="valuewma" class="form-control">
   <option value="3" <?php //($valuewma==3)?'selected':''; ?>>Per 3 Bulan</option>
   <option value="5" <?php //($valuewma==5)?'selected':''; ?>>Per 5 Bulan</option>
  </select-->
  <label>Pilih Barang</label>
  <select name="id_barang" class="form-control">
   <?php foreach($barang as $key=>$brg) { ?>
   <option value="<?= $key ?>" <?= ($key==$id_barang)?'selected':''; ?>><?= $brg['nama_barang'] ?></option>
   <?php } ?>
  </select>
</div>
</div>
<div class="row uniform">
<div class="4u 12u$">
  <ul class="actions">
   <li><button type="submit" name="prediksi" class="button special">Lakukan Peramalan</button></li>
  </ul>
</div>
</div>
</form>
<div class="12u$">
 <?php if($id_barang !== ""){ ?>
 <h2>Peramalan Total Penjualan "<?= $barang[$id_barang]['nama_barang'] ?>" Periode <?= $global_bulan[$bulan]." ".$tahun ?></h2>
 <div class="table-wrapper">

  <table class="alt">
   <thead>
    <tr>
     <th>ID Barang</th>
     <th>Nama Barang</th>
     <th>Hasil Ramal dgn WMA-3</th>
     <th>Hasil Ramal dgn WMA-5</th>
    </tr>
   </thead>
   <tbody>
    <tr>
     <td><?= $id_barang ?></td>
     <td><?= $barang[$id_barang]['nama_barang'] ?></td>
     <td><?= round($barang[$id_barang]['prediksi_jual'][3]) ?></td>
     <td><?= round($barang[$id_barang]['prediksi_jual'][5]) ?></td>
    </tr>
   </tbody>
  </table>
 </div>
 <?php } ?>
</div>
<div class="row uniform">
 <div class="12u$">
  <canvas id='chartaja'></canvas>
 </div>
</div>
<?php
include('footer.php');