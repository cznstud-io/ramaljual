<?php
include_once('conf/koneksi.php');
include('header.php');
?>
<div class="4u$ 12u$(small)">
 <ul class="actions">
  <li><a href="form_barang.php" class="button special"><span class="fa fa-plus"></span> Tambah Barang</a></li>
 </ul>
</div>
<div class="12u$">
 <div class="table-wrapper">
  <table class="alt">
   <thead>
    <tr>
     <th>ID Barang</th>
     <th>Nama Barang</th>
     <th>Harga</th>
     <th>Aksi</th>
    </tr>
   </thead>
   <tbody>
    <?php 
    $query = mysqli_query($kon, "select * from barang order by substring(nama_barang, -5, 3) asc");
    while($row=mysqli_fetch_assoc($query)){ ?>
    <tr>
     <td><?= $row['id_barang'] ?></td>
     <td><?= $row['nama_barang'] ?></td>
     <td>Rp <?= number_format($row['harga_barang'],0,',','.') ?></td>
     <td><a href="form_barang.php?edit=<?= $row['id_barang'] ?>"><span class="fa fa-pencil"></span> Ubah</a> | <a href="hapus.php?table=barang&id=<?= $row['id_barang'] ?>" onclick="return confirm('Hapus barang ini?');"><span class="fa fa-close"></span> Hapus</a>
    </tr>
    <?php } ?>
   </tbody>
  </table>
 </div>
</div>
<?php
include('footer.php');