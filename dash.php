<?php
include('conf/koneksi.php');
include('header.php');
echo "<h2>Selamat Datang di Sistem Peramalan Penjualan Menggunakan Metode Weight Moving Average</h2>";
?>
<style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 100%;
      max-height: 500px;
      margin: auto;
  }
  #myCarousel {
   height: 500px;
  }
  </style>
<div class="row">
 <div class="col-sm-12">
 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
     
     <div class="carousel-inner" role="listbox">
      <?php
       $ctx = 0;
       $folder = new DirectoryIterator("assets/images/slide");
       foreach($folder as $img) {
        if(!$img->isDot()){
         // file starts from index 2, the first two was dot
       ?>
       <div class="item <?= ($ctx===2)?"active":""; ?>">
        <img class="img img-responsive" src="<?= "assets/images/slide/".$img->getFilename(); ?>" />
       </div>
        <?php } 
        $ctx++;
       } ?>
     </div>
     <ol class="carousel-indicators">
       <?php 
       foreach(range(0,$ctx-3) as $ix){ ?>
       <li data-target="#myCarousel" data-slide-to="<?= $ix ?>" <?= ($ix===0)?"class='active'":'' ?>></li>
       <?php } ?>
     </ol>
     <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
       <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
       <span class="sr-only">Previous</span>
     </a>
     <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
       <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
       <span class="sr-only">Next</span>
     </a>
    </div>
<?php
include('footer.php');
?>