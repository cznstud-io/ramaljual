<?php
include_once('conf/koneksi.php');
if(isset($_GET['edit'])){
 $query = mysqli_query($kon, "select id_faktur, DATE(tgl_faktur) as tgl_faktur_f,id_pelanggan, jenis_faktur, total_faktur from faktur where id_faktur='".$_GET['edit']."'");
 $row = mysqli_fetch_assoc($query);
 
} else if (isset($_POST['id_barang'])){
 if($_POST['id_faktur'] === ''){
  $query = mysqli_query($kon, "insert into faktur values('','".$_POST['tgl_faktur']."','".$_POST['id_pelanggan']."','".$_POST['jenis_faktur']."','".$_POST['total_faktur']."','1')");
  //echo "insert into faktur values('','".date('Y-m-d H:i:s')."','".$_POST['id_pelanggan']."','".$_POST['jenis_faktur']."','".$_POST['total_faktur']."','1') <br>";
  if($query){
   $idf = mysqli_insert_id($kon);
   //$idf = 1;
   $que = "insert into faktur_detail values ";
   $count = 0;
   foreach($_POST['id_barang'] as $key => $idx){
    $que .= "('','".$idf."','".$_POST['id_barang'][$key]."','".$_POST['qty'][$key]."')";
    if(isset($_POST['id_barang'][$key+1])){
     $que .= ",";
    }
   }
   $querydetail = mysqli_query($kon, $que);
   //print_r($_POST);
   //echo $que;
   if($querydetail){
    echo "<script>alert('Data berhasil ditambahkan!');\n document.location = 'lihat_faktur.php'</script>";
   } else {
    echo "<script>alert('Terdapat Kesalahan Penambahan. Kode Error: '".mysqli_error($kon).");\n document.location = 'lihat_faktur.php'</script>";
   }
  } else {
   // kalo gagal
  }
 } else {
  $query = mysqli_query($kon,"update faktur set tgl_faktur='".$_POST['tgl_faktur']."', id_pelanggan='".$_POST['id_pelanggan']."', jenis_faktur='".$_POST['jenis_faktur']."', total_faktur='".$_POST['total_faktur']."' where id_faktur='".$_POST['id_faktur']."'");
  //echo mysqli_error($kon);
  //exit();
  if($query){
   mysqli_query($kon, "delete from faktur_detail where id_faktur = '".$_POST['id_faktur']."'");
   $que = "insert into faktur_detail values ";
   $count = 0;
   foreach($_POST['id_barang'] as $key => $idx){
    $que .= "('','".$_POST['id_faktur']."','".$_POST['id_barang'][$key]."','".$_POST['qty'][$key]."')";
    if(isset($_POST['id_barang'][$key+1])){
     $que .= ",";
    }
   }
   $querydetail = mysqli_query($kon, $que);
   if($querydetail){
    echo "<script>alert('Data berhasil diperbarui');\n document.location = 'lihat_faktur.php'</script>";
   }
  } else {
   echo "<script>alert('Terdapat Kesalahan dalam pembaruan data. Kode Error: '".mysqli_error($kon).");\n document.location = 'lihat_faktur.php'</script>";
  }
 }
}
include('header.php');

?>

<form method="POST" action="form_faktur.php" name="kategori">
 <input type="hidden" name="id_faktur" value="<?= isset($row['id_faktur'])?$row['id_faktur']:'' ?>" />
 <div class="row uniform">
  <div class="8u$ 12u$(small)">
   <label>Jenis Faktur</label>
   <select name="jenis_faktur" id="jenis_faktur">
    <option value="0" <?= isset($row['jenis_faktur'])?($row['jenis_faktur']==0?'selected':''):'' ?>>Barang Masuk</option>
    <option value="1" <?= isset($row['jenis_faktur'])?($row['jenis_faktur']==1?'selected':''):'' ?>>Barang Keluar</option>
   </select>
  </div>
  <div class="8u$ 12u$(small)">
   <label>Tanggal Faktur</label>
   <input type="date" name="tgl_faktur" value="<?= isset($row['tgl_faktur_f'])?$row['tgl_faktur_f']:'' ?>" placeholder="Tanggal Faktur" required/>
  </div>
  <div class="8u$ 12u$(small)">
   <label>Supplier/Pelanggan</label>
   <input type="text" name="id_pelanggan" value="<?= isset($row['id_pelanggan'])?$row['id_pelanggan']:'' ?>" placeholder="Nama Supplier/Pelanggan" required/>
  </div>
  <div class="12u 12u$">
   <script>
   <?php
   $query = mysqli_query($kon, "select * from barang");
   $barang = array();
   while($row=mysqli_fetch_assoc($query)){
    $barang[$row['id_barang']] = $row;
   }
   
   $query = mysqli_query($kon, "select masuk.id_barang,masuk.nama_barang, masuk.total_masuk,keluar.total_keluar, (masuk.total_masuk - keluar.total_keluar) as stok from 
    (SELECT barang.id_barang,barang.nama_barang,sum(qty) as total_masuk FROM `faktur_detail`,barang,faktur where faktur.id_faktur = faktur_detail.id_faktur and barang.id_barang = faktur_detail.id_barang and faktur.jenis_faktur = 0 group by faktur_detail.id_barang) as masuk,
    (SELECT barang.id_barang,barang.nama_barang,sum(qty) as total_keluar FROM `faktur_detail`,barang,faktur where faktur.id_faktur = faktur_detail.id_faktur and barang.id_barang = faktur_detail.id_barang and faktur.jenis_faktur = 1 group by faktur_detail.id_barang) as keluar 
   where masuk.id_barang = keluar.id_barang");
   $stok=array();
   while($row=mysqli_fetch_assoc($query)){
    $barang[$row['id_barang']]['stok'] = $row['stok'];
    $barang[$row['id_barang']]['terpilih'] = false;
   }
   // default data for editing
   $faktur_detail = array();
   if(isset($_GET['edit'])){
    $querydetail = mysqli_query($kon, "select faktur_detail.*, barang.nama_barang, barang.harga_barang from faktur_detail,barang where faktur_detail.id_barang = barang.id_barang and faktur_detail.id_faktur = '".$_GET['edit']."'");
    while($row=mysqli_fetch_assoc($querydetail)){
     $faktur_detail[] = array(
      'id_barang' => $row['id_barang'],
      'nama_barang'=> $row['nama_barang'],
      'harga_barang' => $row['harga_barang'],
      'qty' => $row['qty'],
     );
    }
    echo "faktur_detail = ".json_encode($faktur_detail)."; \n";
   } else {
    echo "faktur_detail = []; \n";
   }
   
   
   echo "barang = ".json_encode($barang);
   ?>
   </script>
   <table class="alt">
    <thead>
     <tr>
      <th width="100px"></th>
      <th>Nama Barang</th>
      <th width="200px">Harga Satuan</th>
      <th width="100px">Kuantitas</th>
      <th width="200px">Total</th>
     </tr>
    </thead>
    <tbody id="isiform">
    </tbody>
    <tfoot>
     <tr>
      <td colspan="4">Total Akhir</td>
      <td>
       <input type="hidden" name="total_faktur" id="total_faktur" value=0 />
       <span id="tx_total_faktur"></span>
      </td>
     </tr>
    </tfoot>
   </table>
   <button type="button" class="button special" id="btn_tambahdata"><span class="fa fa-add"></span> Tambah Barang</button>
  </div>
  <ul class="actions">
   <li><button type="submit" name="simpan" class="button special">Simpan</button></li>
  </ul>
 </div>
</form>
<?php ob_start(); ?>
<script>
 var rowcount = 0, total_fk = 0;
 
  function barang_option(selected_val=''){
   barang_option_html = "<option selected disabled>Pilih Barang</option> \n";
   Object.keys(barang).forEach(function(el) {
     st = (selected_val === el)?'selected':'';
     barang_option_html += "<option value='"+el+"' "+st+">"+barang[el].nama_barang+"</option> \n";
   });
   return barang_option_html;
  }
 function addForm(id_barang='',qty=1){
  html_opt_barang = barang_option(id_barang);
  var isiform =  '<tr class="isi" id="row'+rowcount+'">'+
  '<td><button type="button" class="button small" onclick="hapusRow('+rowcount+')"><span class="fa fa-close"></span></button></td>'+
  '<td><select name="id_barang[]" rowid="'+rowcount+'" class="form-control" onchange="upharga(this)" required>'+html_opt_barang+'</select></td>'+
  '<td><span id="harga'+rowcount+'">-</span></td>'+
  '<td><input type="text" name="qty[]" class="form-control qty" id="qty'+rowcount+'" placeholder="Qty" barangid="" rowid="'+rowcount+'" value="'+qty+'" onchange="uptotal(this)" required /><br><strong id="stokbr'+rowcount+'"></strong></td>'+
  '<td><span id="tx_total'+rowcount+'">-</span><input type="hidden" id="total'+rowcount+'" name="total_brg" class="total_brg" value=0 /></td></tr>';
  $("#isiform").append(isiform);
  return rowcount++;
 }
 function upharga(elem){
  id = elem.getAttribute('rowid');
  console.log(elem.value);
  $("#harga"+id).text(barang[elem.value].harga_barang);
  //$("#qty"+id).val(1);
  $("#qty"+id).attr('barangid',elem.value);
  $("#stokbr"+id).text("Stok : "+barang[elem.value]['stok']);
  $("#total"+id).val(barang[elem.value].harga_barang*1);
  $("#tx_total"+id).text(barang[elem.value].harga_barang*1);
  //$(elem).closest("td>.qty").val("1");
  barang[elem.value]['terpilih'] = true;
  uptotal_all();
 }
 function uptotal(elem){
  id = elem.getAttribute('rowid');
  id_barang = elem.getAttribute('barangid');
  if($("#jenis_faktur").val() == '1' && barang[id_barang]['stok'] < parseInt($("#qty"+id).val())){
   alert('Stok '+barang[id_barang]['nama_barang']+' tidak mencukupi');
   $("#qty"+id).val(barang[id_barang]['stok']);
  }
  total = parseInt($("#harga"+id).text()) * parseInt($("#qty"+id).val());
  $("#total"+id).val(total);
  $("#tx_total"+id).text(total);
  uptotal_all();
 }
 function uptotal_all(){
  total_fk = 0;
  $(".total_brg").each(function() {
   total_fk = total_fk + parseInt($(this).val());
   $("#total_faktur").val(total_fk);
   $("#tx_total_faktur").text(total_fk);
  });
 }
 function hapusRow(id){
  $("#row"+id).remove();
  uptotal_all();
 }
 $(document).ready(function() {
  $("#btn_tambahdata").click(function() {
   addForm();
  });
  $("input.qty").change(function(elem) {
   console.log($(this).value);
  })
  $("tr.isi").each(function(){
   $this = $(this);
   this_sel = $(this)[0].id;
   $("#"+this_sel+" idbr").change(function() {
    console.log($(this).val());
   });
  });
  if(faktur_detail.length !== 0){
   Object.keys(faktur_detail).forEach(function(el) {
    item = faktur_detail[el]; 
    rowid = addForm(item.id_barang,item.qty);
    upharga($("select[rowid="+rowid+"]")[0]);
    uptotal(document.getElementById("qty"+rowid));
   });
  }
 });
</script>
<?php
$vws->set_inline(ob_get_clean());
include('footer.php');