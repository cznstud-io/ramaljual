<?php
/*
 * WMAverage - Peramalan menggunakan rumus Weighted 
 * Moving Average
 * 
 * Daftar Fungsi:
 * - fit(array $data)
 *     Mengatur data latih untuk digunakan prediksi. Data latih
 *     harus dalam bentuk array dan hanya disimpan untuk 
 *     dipakai pada prediksi.
 *     Return Value : Tidak Ada
 *  
 * - predict(int $data_index [,int $weight = 0])
 *     Prediksi data ke $data_index. Jumlah data yang digunakan
 *     sebagai data latih adalah sebanyak $weight. Jika $weight
 *     tidak ditentukan, maka $weight ditentukan sebesar jumlah
 *     data latih. 
 *     Return Value: [mixed $hasil] apabila $data_index berada
 *     dalam range data yang bisa diprediksi ($data_index lebih 
 *     besar dari $weight) 
 */
 
class WMAverage{
 
 private $traindata;
 private $weighted = array();
 
 function fit($data){
  $this->traindata = $data;
 }
 
 function predict($data_index, $weight=0){
  $cdata = count($this->traindata);
  $weight = ($weight!==0)?$weight:$cdata;
  if($data_index > $weight || $data_index == ($cdata +1) ){
   $sumWData = 0;
   $sumWeight = 0;
   //$data = array_slice(array_reverse($this->traindata), ($cdata-1)-($data_index-2), $weight);
   //$data = $this->traindata;
   foreach($this->traindata as $x){
    //echo $x." * ".$weight."<br>";
    $sum = $x * $weight;
    $this->weighted[] = $sum;
    $sumWData += $sum;
    $sumWeight += $weight--;
    if($weight===0)
     break;
   }
   //echo $sumWData." / ".$sumWeight."<br>";
   $hasil = $sumWData / $sumWeight;
   return $hasil;
  } else {
   return false;
  } 
 }
}

// Contoh Pengujian 
$wma = new WMAverage();
/*$wma->fit([331,496,540,487]);
$bln=13;
 echo "Bulan ".$bln." <br>";
 echo var_dump($wma->predict($bln,4));*/