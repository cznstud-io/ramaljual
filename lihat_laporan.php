<?php
include_once('conf/koneksi.php');
$barang = array();
$querybarang = mysqli_query($kon,"select id_barang,nama_barang from barang order by substring(nama_barang, -5, 3) asc");
while($row=mysqli_fetch_array($querybarang)){
 $barang[$row['id_barang']] = array(
  'nama_barang' => $row['nama_barang'],
  'stok_awal' => 0,
  'stok_masuk' => 0,
  'stok_keluar' => 0
 );
}
$bulan = isset($_POST['bulan']) ? $_POST['bulan'] : date('m');
$tahun = isset($_POST['tahun']) ? $_POST['tahun'] : date('Y');
$queryawal =  mysqli_query($kon,"SELECT barang.id_barang,barang.nama_barang,(sum(if(faktur.jenis_faktur=0,qty,0)) - sum(if(faktur.jenis_faktur=1,qty,0))) as stok FROM faktur_detail right join barang on faktur_detail.id_barang = barang.id_barang,faktur where faktur.id_faktur = faktur_detail.id_faktur and barang.id_barang = faktur_detail.id_barang and (YEAR(faktur.tgl_faktur) * 12 + MONTH(faktur.tgl_faktur) - 1) < (".$tahun." * 12 + ". (int)$bulan ." - 1) group by faktur_detail.id_barang");
// tricky date criteria provided by "Gordon Linoff" in stackoverflow.com/questions/27953473 with some modification
while($row=mysqli_fetch_array($queryawal)){
 $barang[$row['id_barang']]['stok_awal'] = $row['stok'];
}
$querymasuk = mysqli_query($kon,"SELECT barang.id_barang,barang.nama_barang,sum(qty) as total_masuk FROM `faktur_detail`,barang,faktur where faktur.id_faktur = faktur_detail.id_faktur and barang.id_barang = faktur_detail.id_barang and faktur.jenis_faktur = 0 and MONTH(faktur.tgl_faktur) = '".$bulan."' and YEAR(faktur.tgl_faktur) = ".$tahun." group by faktur_detail.id_barang");
while($row=mysqli_fetch_array($querymasuk)){
 $barang[$row['id_barang']]['stok_masuk'] = $row['total_masuk'];
}
$querykeluar = mysqli_query($kon, "SELECT barang.id_barang,barang.nama_barang,sum(qty) as total_keluar FROM `faktur_detail`,barang,faktur where faktur.id_faktur = faktur_detail.id_faktur and barang.id_barang = faktur_detail.id_barang and faktur.jenis_faktur = 1 and MONTH(faktur.tgl_faktur) = '".$bulan."' and YEAR(faktur.tgl_faktur) = ".$tahun." group by faktur_detail.id_barang");
while($row=mysqli_fetch_array($querykeluar)){
 $barang[$row['id_barang']]['stok_keluar'] = $row['total_keluar'];
}

include('header.php');

?>
<script>
 data = {
  labels: [ <?php $count = 1; foreach($barang as $val){ echo "'".$val['nama_barang']."'"; echo ($count++ == count($barang)?'':', '); } ?> ],
  datasets : [
    {
     label: "Stok Awal",
     backgroundColor : "rgba(200, 80, 80, 0.7)",
     borderColor : "rgba(50,50,50, 0.8)",
     borderWidth : 1.0,
     data : [ <?php $count = 1; foreach($barang as $val){ echo "'".$val['stok_awal']."'"; echo ($count++ == count($barang)?'':', '); } ?> ]
    },
    {
     label: "Stok Masuk",
     backgroundColor : "rgba(80, 200, 120, 0.7)",
     borderColor : "rgba(50,50,50, 0.8)",
     borderWidth : 1.0,
     data : [ <?php $count = 1; foreach($barang as $val){ echo "'".$val['stok_masuk']."'"; echo ($count++ == count($barang)?'':', '); } ?> ]
    },
    {
     label: "Stok Keluar",
     backgroundColor : "rgba(80, 80, 200, 0.7)",
     borderColor : "rgba(50,50,50, 0.8)",
     borderWidth : 1.0,
     data : [ <?php $count = 1; foreach($barang as $val){ echo "'".$val['stok_keluar']."'"; echo ($count++ == count($barang)?'':', '); } ?> ]
    },
  ]
 }
</script>
<form name="pilihtgl" action="" method="POST">
<div class="row uniform">
<div class="4u 12u">
  <label>Pilih Bulan Laporan</label>
  <select name="bulan" class="form-control">
   <?php foreach($global_bulan as $key=>$val){ ?>
   <option value="<?= $key ?>" <?= ($bulan==$key)?'selected':''; ?>><?= $val ?></option>
   <?php } ?>
  </select>
  
</div>
<div class="4u 12u$">
  <label>Pilih Tahun Laporan</label>
  <select name="tahun" class="form-control">
   <?php foreach(range(2016,date('Y')+5) as $val){ ?>
   <option value="<?= $val ?>" <?= ($tahun==$val)?'selected':''; ?>><?= $val ?></option>
   <?php } ?>
  </select>
</div>
</div>
<div class="row uniform">
<div class="4u 12u$">
  <ul class="actions">
   <li><button type="submit" name="cari" class="button special">Lihat Laporan</button></li>
  </ul>
</div>
</div>
</form>
<div class="row uniform">
<div class="12u$">
 <h2>Laporan Stok Bulanan Periode <?= $global_bulan[$bulan]." ".$tahun ?></h2>
 <div class="table-wrapper">
  <table class="alt">
   <thead>
    <tr>
     <th>ID Barang</th>
     <th>Nama Barang</th>
     <th>Stok Awal</th>
     <th>Stok Masuk</th>
     <th>Stok Keluar</th>
     <th>Total</th>
    </tr>
   </thead>
   <tbody>
    <?php foreach($barang as $key=>$val){ ?>
    <tr>
     <td><?= $key ?></td>
     <td><?= $val['nama_barang'] ?></td>
     <td><?= $val['stok_awal'] ?></td>
     <td><?= $val['stok_masuk'] ?></td>
     <td><?= $val['stok_keluar'] ?></td>
     <td><?= $val['stok_awal'] + $val['stok_masuk'] - $val['stok_keluar'] ?></td>
    </tr>
    <?php } ?>
   </tbody>
  </table>
 </div>
</div>
</div>
<div class="row uniform">
 <div class="12u$">
  <canvas id='chartaja'></canvas>
 </div>
</div>
<?php
ob_start();
?>
<script src="assets/js/Chart.min.js"></script>
<script>
  $(document).ready(function() {
   var ctx=document.getElementById('chartaja').getContext('2d');
   window.myBar=new Chart(ctx,{
    type:'bar',
    data:data,
    options:{
     responsive:true,
     legend:{
      display:false,
      position:'top'
     },
     title:{
      display:true,
      text:'Laporan Stok Bulanan Periode <?= $global_bulan[$bulan]." ".$tahun ?>'
     },
     scales: {
      yAxes: [{
       ticks: {
        min: 0,
        stepsize: 10
       }
      }]
     }
    }
   });
  });
</script>
<?php
$vws->set_inline(ob_get_clean());
include('footer.php');