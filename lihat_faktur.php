<?php
include_once('conf/koneksi.php');
include('header.php');
$faktur = [];
$query = mysqli_query($kon, "select faktur.*, sum(faktur_detail.qty) as jumlahqty from faktur,faktur_detail where faktur.id_faktur = faktur_detail.id_faktur group by faktur_detail.id_faktur order by tgl_faktur desc");
 while($row=mysqli_fetch_assoc($query)){
  if($row['jenis_faktur'] == 0){
   $faktur['masuk'][] = $row;
  } else {
   $faktur['keluar'][] = $row;
  }
 }
?>
<div class="row uniform">
 <div class="4u$ 12u$(small)">
  <ul class="actions">
   <li><a href="form_faktur.php" class="button special"><span class="fa fa-plus"></span> Tambah Faktur</a></li>
  </ul>
 </div>
</div>
<div class="row uniform">
 <div class="12u$">
  <ul class="nav nav-tabs">
   <li class="active"><a data-toggle="tab" href="#keluar"><span class="fa fa-upload"></span> Faktur Keluar</a></li>
   <li><a data-toggle="tab" href="#masuk"><span class="fa fa-download"></span> Faktur Masuk</a></li>
  </ul>
  <div class="tab-content">
   <div id="keluar" class="tab-pane fade in active">
    <div class="table-wrapper">
     <table class="alt">
      <thead>
       <tr>
        <th>Nomor Faktur</th>
        <th>Tanggal Faktur</th>
        <th>Nama Outlet</th>
        <th>Jumlah Keluar</th>
        <th>Aksi</th>
       </tr>
      </thead>
      <tbody>
       <?php 
       foreach($faktur['keluar'] as $row){ ?>
       <tr>
        <td><?= $row['id_faktur'] ?></td>
        <td><?= tanggal($row['tgl_faktur']) ?></td>
        <td><?= $row['id_pelanggan'] ?></td>
        <td><?= $row['jumlahqty'] ?></td>
        <td><a href="lihat_faktur_detail.php?id=<?= $row['id_faktur'] ?>"><span class="fa fa-eye"></span> Lihat</a> | <a href="form_faktur.php?edit=<?= $row['id_faktur'] ?>"><span class="fa fa-pencil"></span> Ubah</a> | <a href="hapus.php?table=faktur&id=<?= $row['id_faktur'] ?>" onclick="return confirm('Hapus faktur ini?');"><span class="fa fa-close"></span> Hapus</a></td>
       </tr>
       <?php } ?>
      </tbody>
     </table>
    </div>
   </div>
   <div id="masuk" class="tab-pane fade in">
    <div class="table-wrapper">
     <table class="alt">
      <thead>
       <tr>
        <th>Nomor Faktur</th>
        <th>Tanggal Faktur</th>
        <th>Jumlah Masuk</th>
        <th>Aksi</th>
       </tr>
      </thead>
      <tbody>
       <?php 
       foreach($faktur['masuk'] as $row){ ?>
       <tr>
        <td><?= $row['id_faktur'] ?></td>
        <td><?= tanggal($row['tgl_faktur']) ?></td>
        <td><?= $row['jumlahqty'] ?></td>
        <td><a href="lihat_faktur_detail.php?id=<?= $row['id_faktur'] ?>"><span class="fa fa-eye"></span> Lihat</a> | <a href="form_faktur.php?edit=<?= $row['id_faktur'] ?>"><span class="fa fa-pencil"></span> Ubah</a> | <a href="hapus.php?table=faktur&id=<?= $row['id_faktur'] ?>" onclick="return confirm('Hapus faktur ini?');"><span class="fa fa-close"></span> Hapus</a></td>
       </tr>
       <?php } ?>
      </tbody>
     </table>
    </div>
   </div>
  </div>
 </div>
</div>
<?php
include('footer.php');