								</section>

						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Senna &amp; Nixxa</h2>
									</header>
									<ul>
          <li><a href="dash.php"><span class="fa fa-home" style="display:inline-block !important;font-size:1.5em;padding-right:2px"></span>&nbsp;&nbsp; Home</a></li>
          <?php if($_SESSION['level'] == 0){ ?>
          <li><a href="lihat_barang.php"><span class="fa fa-glass" style="display:inline-block !important;font-size:1.5em;padding-right:2px"></span>&nbsp;&nbsp;Kelola Barang</a></li>
          <li><a href="lihat_faktur.php"><span class="fa fa-clipboard" style="display:inline-block !important;font-size:1.5em;padding-right:2px"></span>&nbsp;&nbsp;Kelola Faktur</a></li>
          <li><a href="lihat_pengguna.php"><span class="fa fa-user" style="display:inline-block !important;font-size:1.5em;padding-right:2px"></span>&nbsp;&nbsp;Kelola Karyawan</a></li>
          <?php } ?>
          <li><a href="lihat_laporan.php"><span class="fa fa-file-excel-o" style="display:inline-block !important;font-size:1.5em;padding-right:2px"></span>&nbsp;&nbsp;Lihat Laporan</a></li>
          <li><a href="lihat_peramalan.php"><span class="fa fa-hourglass-half" style="display:inline-block !important;font-size:1.5em;padding-right:2px"></span>&nbsp;&nbsp;Peramalan</a></li>
          <?php if($_SESSION['level'] == 1){ ?>
          <li><a href="form_pengguna.php?edit=<?= $_SESSION['id_user'] ?>"><span class="fa fa-user" style="display:inline-block !important;font-size:1.5em;padding-right:2px"></span>&nbsp;&nbsp;Ubah Data Saya</a></li>
          <li><a href="form_password.php?id=<?= $_SESSION['id_user'] ?>"><span class="fa fa-key" style="display:inline-block !important;font-size:1.5em;padding-right:2px"></span>&nbsp;&nbsp;Ubah Password Saya</a></li>
          <?php } ?>
										<li><a href="logout.php"><span class="fa fa-sign-out" style="display:inline-block !important;font-size:1.5em;padding-right:2px"></span> Keluar</a></li>
									</ul>
								</nav>
        
							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; PT Pratama Abadi Gemilang. Desain Oleh <a href="https://html5up.net">HTML5 UP</a>.</p>
								</footer>

						</div>
					</div>

			</div>

		<!-- Scripts >
			
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]>
			<script src="assets/js/main.js"></script-->
   
  <script src="assets/js/bootstrap.min.js"></script>
   <?= $vws->get_inline(); ?>
	</body>
</html>