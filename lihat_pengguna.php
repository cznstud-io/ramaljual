<?php
include_once('conf/koneksi.php');
include('header.php');
?>
<div class="4u$ 12u$(small)">
 <ul class="actions">
  <li><a href="form_pengguna.php" class="button special"><span class="fa fa-plus"></span> Tambah Pengguna</a></li>
 </ul>
</div>
<div class="12u$">
 <div class="table-wrapper">
  <table class="alt">
   <thead>
    <tr>
     <th>Nama Karyawan</th>
     <th>Jabatan</th>
     <th>Aksi</th>
    </tr>
   </thead>
   <tbody>
    <?php 
    $query = mysqli_query($kon, "select * from user");
    while($row=mysqli_fetch_assoc($query)){ ?>
    <tr>
     <td><?= $row['nama_pegawai']." (".$row['nama_user'].")" ?></td>
     <td><?= ($row['level'] === '1')?"Pimpinan":"Administrator" ?></td>
     <td><a href="form_pengguna.php?edit=<?= $row['id_user'] ?>"><span class="fa fa-pencil"></span> Ubah</a> | <a href="form_password.php?id=<?= $row['id_user'] ?>"><span class="fa fa-key"></span> Ubah Password</a> | <a href="hapus.php?table=user&id=<?= $row['id_user'] ?>" onclick="return confirm('Hapus pengguna ini?');"><span class="fa fa-close"></span> Hapus</a>
    </tr>
    <?php } ?>
   </tbody>
  </table>
 </div>
</div>
<?php
include('footer.php');