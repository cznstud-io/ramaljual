<?php
include_once('conf/koneksi.php');
$query = mysqli_query($kon, "select * from faktur where id_faktur = '".$_GET['id']."'");
$row = mysqli_fetch_assoc($query);
include('header.php');

?>

 <h3>Faktur #<?= $row['id_faktur'] ?></h3>
 <div class="row uniform">
  <div class="4u 12u$(small)">
   <label>Jenis Faktur</label>
   <span><?= ($row['jenis_faktur']==1)?"Faktur Keluar":"Faktur Masuk" ?></span>
  </div>
  <div class="4u 12u$(small)">
   <label>Tanggal Faktur</label>
   <span><?= tanggal($row['tgl_faktur']) ?></span>
  </div>
  <div class="4u 12u$(small)">
   <label>Supplier/Pelanggan</label>
   <span><?= $row['id_pelanggan'] ?></span>
  </div>
 </div>
 <div class="row uniform">
  <div class="12u 12u$">
   <?php
    $faktur_detail = array();
    $querydetail = mysqli_query($kon, "select faktur_detail.*, barang.nama_barang, barang.harga_barang from faktur_detail,barang where faktur_detail.id_barang = barang.id_barang and faktur_detail.id_faktur = '".$_GET['id']."'");
    while($row=mysqli_fetch_assoc($querydetail)){
     $faktur_detail[] = array(
      'id_barang' => $row['id_barang'],
      'nama_barang'=> $row['nama_barang'],
      'harga_barang' => $row['harga_barang'],
      'qty' => $row['qty'],
     );
    }
    $total_akhir = 0;
    ?>
   <table class="alt">
    <thead>
     <tr>
      <th>Nama Barang</th>
      <th width="100px">Kuantitas</th>
      <th width="200px">Total</th>
     </tr>
    </thead>
    <tbody id="isiform">
     <?php foreach($faktur_detail as $row) { 
      $total_akhir += $row['qty']*$row['harga_barang'];
     ?>
     <tr>
      <td><?= $row['nama_barang'] ?></td>
      <td><?= $row['qty'] ?></td>
      <td>Rp <?= number_format($row['qty']*$row['harga_barang'],0,',','.'); ?></td>
     </tr>
     <?php } ?>
    </tbody>
    <tfoot>
     <tr>
      <td colspan="2">Total Akhir</td>
      <td>
       <span id="tx_total_faktur">Rp <?= number_format($total_akhir,0,',','.') ?></span>
      </td>
     </tr>
    </tfoot>
   </table>
  </div>
  <ul class="actions">
   <li><a href="#" onclick="window.history.back()" class="button special"><span class="fa fa-chevron-left"></span> Kembali</a></li>
   <li><a href="form_faktur.php?edit=<?= $_GET['id'] ?>" class="button special"><span class="fa fa-pencil"></span> Ubah Faktur</a></li>
   <li><a href="hapus.php?table=faktur&id=<?= $_GET['id'] ?>" class="button special" onclick="return confirm('Hapus faktur ini?')"><span class="fa fa-close"></span> Hapus Faktur</a></li>
  </ul>
 </div>
<?php include('footer.php'); ?>